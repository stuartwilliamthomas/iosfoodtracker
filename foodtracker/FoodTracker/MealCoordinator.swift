//
//  MealCoordinator.swift
//  FoodTracker
//
//  Created by Stuart Thomas on 11/02/2021.
//  Copyright © 2021 Apple Inc. All rights reserved.
//

import Foundation
import UIKit

class MealCoordinator {
  
  weak var controller: UIViewController?
  
  init(controller: UIViewController) {
    self.controller = controller
  }
  
  func openMeal(for meal: Meal) {
    let mealViewControllerStoryboard = UIStoryboard(name: "MealViewController", bundle: .main)
    guard let mealViewController: MealViewController = mealViewControllerStoryboard.instantiateInitialViewController() as? MealViewController else { return }
    mealViewController.viewModel = MealViewModel(meal: meal)
    controller?.navigationController?.pushViewController(mealViewController,
                                                         animated: true)
  }
  
}
