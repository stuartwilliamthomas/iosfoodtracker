//
//  MealViewModel.swift
//  FoodTracker
//
//  Created by Stuart Thomas on 11/02/2021.
//  Copyright © 2021 Apple Inc. All rights reserved.
//

import Foundation

class MealsViewModel {
  
  typealias Handler = (Error?) -> ()
  
  var request: RequestType?
  var meals: Meals?
  var mealList: [Meal]?
  
  init(request: RequestType = Request()) {
    self.request = request
  }
  
  func fetchSampleMeals(completion: @escaping Handler) {
    request?.fetchSampleMeals { [weak self] result, error in
      self?.mealList = result?.mealList
      completion(error)
    }
  }
  
  func fetchMeals(completion: @escaping Handler) {
    request?.fetchMeals { [weak self] result, error in
      self?.mealList = result?.mealList
      completion(error)
    }
  }
}
