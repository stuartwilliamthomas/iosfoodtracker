//
//  MealTableViewDataSource.swift
//  FoodTracker
//
//  Created by Stuart Thomas on 11/02/2021.
//  Copyright © 2021 Apple Inc. All rights reserved.
//

import Foundation
import UIKit

class MealTableViewDataSource: NSObject,
                         UITableViewDataSource,
                         UITableViewDelegate {
  
  let tableView: UITableView
  let mealCoordinator: MealCoordinator
  var viewModel: MealsViewModel {
    didSet {
      tableView.reloadData()
    }
  }
  
  init(_ tableView: UITableView,
       _ viewModel: MealsViewModel,
       _ mealCoordinator: MealCoordinator) {
    self.tableView = tableView
    self.viewModel = viewModel
    self.mealCoordinator = mealCoordinator
    super.init()
    tableView.dataSource = self
    tableView.delegate = self
  }
  
  //MARK: - Table view data source
  func numberOfSections(in tableView: UITableView) -> Int {
      return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.mealList?.count ?? 0
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cellIdentifier = "MealTableViewCell"
      
      guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? MealTableViewCell else {
          fatalError("The dequeued cell is not an instance of MealTableViewCell.")
      }
      
      cell.meal = viewModel.mealList?[indexPath.row]
      return cell
  }
  
  func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
      return true
  }
  
  func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
      if editingStyle == .delete {
          viewModel.mealList?.remove(at: indexPath.row)
          let dataService = DataService()
          dataService.saveMeals(viewModel: viewModel)
          tableView.deleteRows(at: [indexPath], with: .fade)
      }
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    guard let meal = viewModel.mealList?[indexPath.row] else { return }
    mealCoordinator.openMeal(for: meal)
  }

}

