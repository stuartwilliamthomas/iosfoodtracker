//
//  DataService.swift
//  FoodTracker
//
//  Created by Stuart Thomas on 11/02/2021.
//  Copyright © 2021 Apple Inc. All rights reserved.
//

import Foundation
import os.log

class DataService {

  func saveMeals(viewModel: MealsViewModel) {
      do {
          guard let unwrappedMealsList = viewModel.mealList else { return }
          let data = try NSKeyedArchiver.archivedData(withRootObject: unwrappedMealsList, requiringSecureCoding: false)
          try data.write(to: Meal.ArchiveURL)
          os_log("Meals successfully saved.", log: OSLog.default, type: .debug)
      } catch {
          os_log("Failed to save meals...", log: OSLog.default, type: .error)
      }
  }
  
}


