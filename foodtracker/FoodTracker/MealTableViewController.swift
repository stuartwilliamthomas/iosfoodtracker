//
//  MealTableViewController.swift
//  FoodTracker
//

import UIKit
import os.log

class MealTableViewController: UIViewController {
  
  @IBOutlet var tableView: UITableView!
  
    //MARK: Properties
    var dataSource: MealTableViewDataSource?
    var viewModel: MealsViewModel!
    
    override func viewDidLoad() {
      viewModel = MealsViewModel()
      dataSource = MealTableViewDataSource(tableView, viewModel, MealCoordinator(controller: self))
      super.viewDidLoad()
      navigationItem.leftBarButtonItem = editButtonItem
      
      fetchMeals()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
  private func fetchSampleMeals() {
    viewModel?.fetchSampleMeals(completion: { [weak self] error in
      if error != nil {
        self?.showRequestError()
      } else {
        DispatchQueue.main.async {
          self?.tableView.reloadData()
        }
      }
    })
  }
  
  private func fetchMeals() {
    viewModel?.fetchMeals(completion: { [weak self] error in
      if error != nil {
        self?.showRequestError()
      } else {
        DispatchQueue.main.async {
          self?.tableView.reloadData()
        }
      }
    })
  }
  
  private func showRequestError() {
    let alert = UIAlertController(title: "Something went wrong with your request",
                                  message: "Do you want to try again?",
                                  preferredStyle: UIAlertController.Style.alert)
    
    alert.addAction(UIAlertAction(title: "Try Again?",
                                  style: .default,
                                  handler: { [weak self] _ in
      self?.fetchMeals()
    }))
    alert.addAction(UIAlertAction(title: "Try Sample Meals?",
                                  style: .default,
                                  handler: { [weak self] _ in
      self?.fetchSampleMeals()
    }))
    alert.addAction(UIAlertAction(title: "Cancel",
                                  style: .cancel))
    DispatchQueue.main.async {
      self.present(alert, animated: true, completion: nil)
    }
  }
  
  @IBAction func didPressAdd(_ sender: Any) {
    os_log("Adding a new meal.", log: OSLog.default, type: .debug)
  }
  

}
