//
//  Meals.swift
//  FoodTracker
//
//  Created by Stuart Thomas on 11/02/2021.
//  Copyright © 2021 Apple Inc. All rights reserved.
//

import Foundation

public struct Meals {
  public let mealList: [Meal]

  public init(mealList: [Meal]) {
      self.mealList = mealList
  }
}
