//
//  MealTableViewCell.swift
//  FoodTracker
//

import UIKit

class MealTableViewCell: UITableViewCell {
  
  //MARK: Properties
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var photoImageView: UIImageView!
  @IBOutlet weak var ratingControl: RatingControl!
  
  var meal: Meal? {
    didSet {
      setupUI()
    }
  }
  
  private func setupUI() {
    guard let meal = meal else { return }
    nameLabel.text = meal.name
    photoImageView.image = meal.photo
    ratingControl.rating = meal.rating 
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }
}
