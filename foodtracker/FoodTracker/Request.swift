//
//  Request.swift
//  FoodTracker
//
//  Created by Stuart Thomas on 11/02/2021.
//  Copyright © 2021 Apple Inc. All rights reserved.
//

import Foundation
import UIKit
import os.log

public protocol RequestType {
  typealias SampleMealsHandler = (Meals?, Error?) -> ()
  typealias MealsHandler = (Meals?, Error?) -> ()
  func fetchSampleMeals(completion: @escaping SampleMealsHandler)
  func fetchMeals(completion: @escaping MealsHandler)
}

public class Request: RequestType {

  public func fetchSampleMeals(completion: @escaping SampleMealsHandler) {
    let photo1 = UIImage(named: "meal1")
    let photo2 = UIImage(named: "meal2")
    let photo3 = UIImage(named: "meal3")
    
    guard let meal1 = Meal(name: "Caprese Salad",
                           photo: photo1,
                           rating: 4,
                           mealDescription: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum") else {
        fatalError("Unable to instantiate meal1")
    }
    
    guard let meal2 = Meal(name: "Chicken and Potatoes",
                           photo: photo2,
                           rating: 5,
                           mealDescription: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum") else {
        fatalError("Unable to instantiate meal2")
    }
    
    guard let meal3 = Meal(name: "Pasta with Meatballs",
                           photo: photo3,
                           rating: 3,
                           mealDescription: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum") else {
        fatalError("Unable to instantiate meal2")
    }
    
    let mealsList = [meal1, meal2, meal3]
    
    let meals: Meals = Meals(mealList: mealsList)
    completion(meals, nil)
  }

  public func fetchMeals(completion: @escaping MealsHandler) {
      do {
          let fileData = try Data(contentsOf: Meal.ArchiveURL)
          let data = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(fileData) as? [Meal]
          let meals: Meals = Meals(mealList: data!)
          completion(meals, nil)
      } catch {
          os_log("Failed to load meals...", log: OSLog.default, type: .error)
          completion(nil, error)
      }
  }

}
