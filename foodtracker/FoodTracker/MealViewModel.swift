//
//  MealViewModel.swift
//  FoodTracker
//
//  Created by Stuart Thomas on 11/02/2021.
//  Copyright © 2021 Apple Inc. All rights reserved.
//

import Foundation
import UIKit

class MealViewModel {
  
  let meal: Meal
  
  var nameText: String {
    return meal.name
  }
  
  var photoImage: UIImage {
    return meal.photo!
  }
  
  var rating: Int {
    return meal.rating
  }
  
  var descriptionText: String {
    return meal.mealDescription
  }
  
  init(meal: Meal) {
    self.meal = meal
  }
  
}
